const jwt = require('express-jwt');
const { secret } = require('config.json');
const db = require('_helpers/db');

//module.exports = authorizeToken;

//function authorizeToken() {
    //return [
        // authenticate JWT token and attach decoded token to request as req.user
        //jwt({ secret, algorithms: ['HS256'] }),

        // attach full user record to request object
        //async (req, res, next) => {
            
            // get user with id from token 'sub' (subject) property
            //const user = await db.User.findByPk(req.user.sub);

            // check user still exists
            //if (!user)
            //    return res.status(401).json({ message: 'Unauthorized' });

            //console.log('req.user :'+user.get());
            // authorization successful
            //req.user = user.get();
            //next();

            //const refreshTokens = await db.RefreshToken.find({ user: user.id });
            //req.user.ownsToken = token => !!refreshTokens.find(x => x.token === token);
            //next();


            /*verifyToken = (req, res, next) => {
            console.log('Authorization:'+req.headers["Authorization"]);
            let token = req.headers["Authorization"];
            console.log('token:'+token);

              if (!token) {
                return res.status(403).send({
                  message: "No token provided!"
                });
              }

              jwt.verify(token, secret, (err, decoded) => {
                if (err) {
                  return res.status(401).send({
                    message: "Unauthorized!"
                  });
                }
                console.log('decoded.id:'+decoded.id);
                //req.user.id = decoded.id;
                next();
              });
            };*/


        //}
   // ];
//}

/*const authJwt = {
  verifyToken: verifyToken
};
module.exports = authJwt;*/




const  verifyToken= (req, res, next) => {
  try {
    const token = req.headers.authorization.split(' ')[1];
    const decodedToken = jwt.verify(token, secret);
    console.log('decodedToken:'+decodedToken);
    console.log('decodedToken:'+decodedToken.userId);

    const userId = decodedToken.userId;
    if (req.body.userId && req.body.userId !== userId) {
      throw 'Invalid user ID';
    } else {
      next();
    }
  } catch {
    res.status(401).json({
      error: new Error('Invalid request!')
    });
  }
};