const jwt = require('express-jwt');
const { secret } = require('config.json');
const db = require('_helpers/db');

module.exports = authorize;

function authorize() {
    console.log('secret:'+secret);
    return [
        // authenticate JWT token and attach decoded token to request as req.user
        
        jwt({ secret, algorithms: ['HS256'] }),
       

        // attach full user record to request object
        async (req, res, next) => {
            console.log('req.user sub:'+req.user.sub);
            // get user with id from token 'sub' (subject) property
            const user = await db.User.findByPk(req.user.sub);

            // check user still exists
            if (!user)
                return res.status(401).json({ message: 'Unauthorized' });

            console.log('req.user :'+user.get());
            // authorization successful
            req.user = user.get();
            console.log('req.user 2:'+ req.user );
            next();
     
        
            //const refreshTokens = await db.RefreshToken.findAll({ where: { user: user.id } });
            //console.log('refreshTokens :'+refreshTokens);
            //req.user.ownsToken = token => !!refreshTokens.find(x => x.token === token);
            //next();
        }
    ];
}