const { DataTypes } = require('sequelize');

module.exports = model;

function model(sequelize) {
    const attributes = {
        CountryName: { type: DataTypes.STRING, allowNull: false },
        GMTOffset: { type: DataTypes.STRING, allowNull: false }
    };

    const options = {
        defaultScope: {
            // exclude hash by default
              attributes: { exclude: ['GMTOffset'] }
              //attributes: {}
        },
        scopes: {
            // include hash with this scope
            withHash: { attributes: {}, }
        }
    };

    //return sequelize.define('User', attributes, options);
    return sequelize.define('CountryDetail', attributes,{timestamps: false,freezeTableName: true, tableName: 'countryDetail'});
}