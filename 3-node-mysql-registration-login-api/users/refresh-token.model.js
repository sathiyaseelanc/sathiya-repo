const { DataTypes } = require('sequelize');

module.exports = model;

function model(sequelize) {
    const attributes = {
        user: { type: DataTypes.STRING, allowNull: true },
        token: { type: DataTypes.STRING, allowNull: true },
        expires: { type: DataTypes.DATE, allowNull: true },
        created: { type: DataTypes.DATE, allowNull: true },
        createdByIp: { type: DataTypes.STRING, allowNull: true },
        revoked: { type: DataTypes.DATE, allowNull: true },
        revokedByIp: { type: DataTypes.STRING, allowNull: true },
        replacedByToken: { type: DataTypes.STRING, allowNull: true }
    };

    const options = {
        defaultScope: {
            // exclude hash by default
            //attributes: { exclude: ['password'] }
            attributes: { exclude: ['replacedByToken'] }
        },
        scopes: {
            // include hash with this scope
            withHash: { attributes: {}, }
        }
    };

    //return sequelize.define('RefreshToken', attributes, options);
    return sequelize.define('RefreshToken', attributes,{timestamps: false,freezeTableName: true, tableName: 'refreshToken'});
}



