﻿const config = require('config.json');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const db = require('_helpers/db');
const crypto = require("crypto");


module.exports = {
    authenticate,
    getAll,
    getById,
    create,
    update,
    delete: _delete,
    refreshToken,
    revokeToken,
    getRefreshTokens,
    getAllCountryDetail,
    getCountryDetailById,
    getCountryDetail
};

/*
async function authenticate({ email, password }) {
    const user = await db.User.scope('withHash').findOne({ where: { email } });

    if (!user || !(await bcrypt.compare(password, user.password)))
        throw 'Email or password is incorrect';

    // authentication successful
    const token = jwt.sign({ sub: user.id }, config.secret, { expiresIn: '5m' });
    return { ...omitHash(user.get()), token };
}*/

async function authenticate({ email, password, ipAddress }) {
    //const user = await db.User.findOne({ email });
    const user = await db.User.findOne({ where: { email } });
    //const user = await db.User.scope('withHash').findOne({ where: { email } });
    console.log('authenticate user:' + user);


    if (!user || !(await bcrypt.compare(password, user.password)))
        throw 'Email or password is incorrect';
    
    console.log('authenticate password passwd:');

    // authentication successful so generate jwt and refresh tokens
    const jwtToken = generateJwtToken(user);

    console.log('authenticate jwtToken:'+jwtToken);
    const refreshToken = generateRefreshToken(user, ipAddress);
    console.log('authenticate refreshToken:'+refreshToken);

    // save refresh token
    await refreshToken.save();

    // return basic details and tokens
    return { 
        ...basicDetails(user.id),
        jwtToken,
        refreshToken: refreshToken.token
    };
}

async function getAllCountryDetail() {
    return await db.CountryDetail.findAll({});
}

async function getCountryDetailById(id) {
        console.log('service getCountryDetailById:'+id);

    return await getCountryDetail(id);
}

async function getCountryDetail(id) {
    const countryDetail = await db.CountryDetail.findByPk(id);
    if (!countryDetail) throw 'countryDetail not found';
    return countryDetail;
}

async function getAll() {
    return await db.User.findAll();
}

async function getById(id) {
    return await getUser(id);
}

async function create(params) {
    // validate
    if (await db.User.findOne({ where: { email: params.email } })) {
        throw 'Email "' + params.email + '" is already taken';
    }

    // hash password
    if (params.password) {
        params.password = await bcrypt.hash(params.password, 10);
    }

    // save user
    await db.User.create(params);
}

async function update(id, params) {
    const user = await getUser(id);

    // validate
    const emailChanged = params.email && user.email !== params.email;
    if (emailChanged && await db.User.findOne({ where: { email: params.email } })) {
        throw 'Email "' + params.email + '" is already taken';
    }

    // hash password if it was entered
    if (params.password) {
        params.password = await bcrypt.hash(params.password, 10);
    }

    // copy params to user and save
    Object.assign(user, params);
    await user.save();

    return omitHash(user.get());
}

async function _delete(id) {
    const user = await getUser(id);
    await user.destroy();
}

// helper functions

async function getUser(id) {
    const user = await db.User.findByPk(id);
    if (!user) throw 'User not found';
    return user;
}

function omitHash(user) {
    const { password, ...userWithoutHash } = user;
    return userWithoutHash;
}

async function refreshToken({ token, ipAddress }) {
    const refreshToken = await getRefreshToken(token);
    console.log('refreshToken fn:'+refreshToken.user);
    const { user } = refreshToken;
    console.log('user fn:'+user);

    // replace old refresh token with a new one and save
    const newRefreshToken = generateRefreshToken(user, ipAddress);
    refreshToken.revoked = Date.now();
    refreshToken.revokedByIp = ipAddress;
    refreshToken.replacedByToken = newRefreshToken.token;
    await refreshToken.save();
    await newRefreshToken.save();

    console.log('user fn1:'+user);
    const user1 = await getUser(user);
    // generate new jwt
    const jwtToken = generateJwtToken(user1);
    console.log('user fn2:'+user1);

    // return basic details and tokens
    return { 
        ...basicDetails(user1.id),
        jwtToken,
        refreshToken: newRefreshToken.token
    };
}

async function revokeToken({ token, ipAddress }) {
    const refreshToken = await getRefreshToken(token);

    // revoke token and save
    refreshToken.revoked = Date.now();
    refreshToken.revokedByIp = ipAddress;
    await refreshToken.save();
}

function generateJwtToken(user) {
    console.log('userId:'+user);
    //const user= getUser(id);

    // create a jwt token containing the user id that expires in 15 minutes
    //return jwt.sign({ sub: user.id, id: user.id }, config.secret, { expiresIn: '5m' });
    return jwt.sign({ sub: user.id}, config.secret, { expiresIn: '5m' });
}

function generateRefreshToken(user, ipAddress) {
    // create a refresh token that expires in 7 days
    return new db.RefreshToken({
        user: user.id,
        token: randomTokenString(),
        expires: new Date(Date.now() + 7*24*60*60*1000),
        createdByIp: ipAddress
    });
}

function randomTokenString() {
    return crypto.randomBytes(40).toString('hex');
}

async function getRefreshTokens(userId) {
    // check that user exists
    await getUser(userId);

    // return refresh tokens for user
    const refreshTokens = await db.RefreshToken.findAll({where: { user: userId }});
    return refreshTokens;
}

function basicDetails(uid) {
    const user=getUser(uid);
    const { id, email } = user;
    return { id, email};
}

async function getRefreshToken(token) {
    console.log('service getRefreshToken token:'+token);
    //const refreshToken = await db.RefreshToken.findOne({ token }).populate('user');
    const refreshToken = await db.RefreshToken.findOne({ where: { token } });//.populate('user');
    
     console.log('service getRefreshToken refreshToken:'+refreshToken.token);
    if (!refreshToken) throw 'Invalid token';
    return refreshToken;
}

