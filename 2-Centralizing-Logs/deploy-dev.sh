# Install AWS Command Line Interface
# https://aws.amazon.com/cli/
apk add --update python3 python3-dev py-pip
pip install awscli --upgrade

docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME

# Set AWS config variables used during the AWS get-login command below
export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY

# Log into AWS docker registry
# The `aws ecr get-login` command returns a `docker login` command with
# the credentials necessary for logging into the AWS Elastic Container Registry
# made available with the AWS access key and AWS secret access keys above.
# The command returns an extra newline character at the end that needs to be stripped out.
$(aws ecr get-login --no-include-email --region $AWS_REGION | tr -d '\r')

# Push the updated Docker container to the AWS registry.
# Using the $CI_ENVIRONMENT_SLUG variable provided by GitLab, we can use this same script
# for all of our environments (production and staging). This variable equals the environment
# name defined for this job in gitlab-ci.yml.
echo $CI_REGISTRY_IMAGE
echo $CI_COMMIT_REF_NAME
echo $AWS_REGISTRY_IMAGE
echo $CI_ENVIRONMENT_SLUG


docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME $AWS_REGISTRY_IMAGE:$CI_ENVIRONMENT_SLUG
docker push $AWS_REGISTRY_IMAGE:$CI_ENVIRONMENT_SLUG

# The AWS registry now has our new container, but our cluster isn't aware that a new version
# of the container is available. We need to create an updated task definition. Task definitions
# always have a version number. When we register a task definition using a name that already
# exists, AWS automatically increments the previously used version number for the task
# definition with that same name and uses it here. Note that we also define CPU and memory
# requirements here and give it a JSON file describing our task definition that I've saved
# to my repository in a aws/ directory.
aws ecs register-task-definition --family project1-task-$CI_ENVIRONMENT_SLUG --requires-compatibilities FARGATE --cpu 512 --memory 1024 --cli-input-json file://aws/project1-task-definition-$CI_ENVIRONMENT_SLUG.json --region $AWS_REGION


# Tell our service to use the latest version of task definition.
#aws ecs update-service --cluster abc-$CI_ENVIRONMENT_SLUG --service abc --task-definition abc-$CI_ENVIRONMENT_SLUG --region $AWS_REGION
aws ecs update-service --cluster da-mservices-$CI_ENVIRONMENT_SLUG --service project1-service-$CI_ENVIRONMENT_SLUG --task-definition project1-task-$CI_ENVIRONMENT_SLUG --region $AWS_REGION



